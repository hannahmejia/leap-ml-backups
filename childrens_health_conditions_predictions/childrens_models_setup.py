#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Apr 11 16:46:49 2021

@author: verge
"""

import pandas as pd
import numpy as np
from kafka import KafkaConsumer, KafkaProducer
from joblib import load
import datetime
# from dependencies import config
import json
import uuid

df=pd.read_csv('/Users/verge/Desktop/healthpredictions/ziplookup.csv')

def lookupdata(zipcode,df):
    test=df.loc[df['ZIP']==zipcode].values
    sdoh_out=np.delete(test, 0)
    # gives 'poverty_rate', 'snap_pct', 'bad_air_pct', 'Presence of Water Violation', '% Severe Housing Problems', 'num_fastfood'
    return sdoh_out

def racelookup(in1):
    if in1 == 'White':
        value = 1
    elif in1 == 'African American':
        value = 2
    elif in1 == 'Hispanic':
        value = 5
    else: value = 3
    return value

def genderreplace(in2):
    if in2 == 'male':
        value=1
    elif in2 =='female':
        value = 2
    else: value=3
    return value

##asthma model creation
df1=pd.read_csv('/Users/verge/Desktop/asthma_model_set.csv')

df2=pd.DataFrame()

for x in range(len(df1)):
    gender=genderreplace(df1.iloc[x][2])
    age=df1.iloc[x][3]
    race=racelookup(df1.iloc[x][4])
    obesity=df1.iloc[x][5]
    sdoh=lookupdata(df1.iloc[x][6],df)
    diag=df1.iloc[x][7]
    a=[gender,age,race, obesity,sdoh[0],sdoh[1],sdoh[2],sdoh[3],sdoh[4],sdoh[5],diag]
    df2=df2.append([a])

#df.columns.values

df2.columns=['gender','age','race','obesity','poverty_rate','snap_pct', 'bad_air_pct',
       'Presence of Water Violation', '% Severe Housing Problems',
       'num_fastfood','diag']


cols=['gender','age','race','obesity','poverty_rate','snap_pct', 'bad_air_pct',
       'Presence of Water Violation', '% Severe Housing Problems',
       'num_fastfood']

X=df2[cols]
X['Presence of Water Violation'].replace(np.NaN, 0, inplace=True)
y=df2[['diag']]

from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split

from joblib import dump, load

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.5, random_state=1)

clf_all = RandomForestClassifier(random_state=1)
clf_all.fit(X_train, y_train)

y_pred_rf_all=clf_all.predict(X_test)
X_test['out']=y_pred_rf_all
X_test['out_real']=y_test
dump(clf_all, '/Users/verge/Desktop/healthcare/asthma_model.joblib')

X_test.to_csv('/Users/verge/Desktop/asthma_model_out.csv')

##add model creation
df1=pd.read_csv('/Users/verge/Desktop/add_model_set.csv')

df2=pd.DataFrame()

for x in range(len(df1)):
    gender=genderreplace(df1.iloc[x][2])
    age=df1.iloc[x][3]
    race=racelookup(df1.iloc[x][4])
    obesity=df1.iloc[x][5]
    sdoh=lookupdata(df1.iloc[x][6],df)
    diag=df1.iloc[x][7]
    a=[gender,age,race, obesity,sdoh[0],sdoh[1],sdoh[2],sdoh[3],sdoh[4],sdoh[5],diag]
    df2=df2.append([a])

#df.columns.values

df2.columns=['gender','age','race','obesity','poverty_rate','snap_pct', 'bad_air_pct',
       'Presence of Water Violation', '% Severe Housing Problems',
       'num_fastfood','diag']


cols=['gender','age','race','obesity','poverty_rate','snap_pct', 'bad_air_pct',
       'Presence of Water Violation', '% Severe Housing Problems',
       'num_fastfood']

X=df2[cols]
X['Presence of Water Violation'].replace(np.NaN, 0, inplace=True)
y=df2[['diag']]

from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split

from joblib import dump, load

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.5, random_state=1)

clf_all = RandomForestClassifier(random_state=1)
clf_all.fit(X_train, y_train)

y_pred_rf_all=clf_all.predict(X_test)
X_test['out']=y_pred_rf_all
X_test['out_real']=y_test
dump(clf_all, '/Users/verge/Desktop/healthcare/add_model.joblib')

X_test.to_csv('/Users/verge/Desktop/add_model_out.csv')

##seizure model creation
df1=pd.read_csv('/Users/verge/Desktop/seizure_model_set.csv')

df2=pd.DataFrame()

for x in range(len(df1)):
    gender=genderreplace(df1.iloc[x][2])
    age=df1.iloc[x][3]
    race=racelookup(df1.iloc[x][4])
    obesity=df1.iloc[x][5]
    sdoh=lookupdata(df1.iloc[x][6],df)
    diag=df1.iloc[x][7]
    a=[gender,age,race, obesity,sdoh[0],sdoh[1],sdoh[2],sdoh[3],sdoh[4],sdoh[5],diag]
    df2=df2.append([a])

#df.columns.values

df2.columns=['gender','age','race','obesity','poverty_rate','snap_pct', 'bad_air_pct',
       'Presence of Water Violation', '% Severe Housing Problems',
       'num_fastfood','diag']


cols=['gender','age','race','obesity','poverty_rate','snap_pct', 'bad_air_pct',
       'Presence of Water Violation', '% Severe Housing Problems',
       'num_fastfood']

X=df2[cols]
X['Presence of Water Violation'].replace(np.NaN, 0, inplace=True)
y=df2[['diag']]

from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split

from joblib import dump, load

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.5, random_state=1)

clf_all = RandomForestClassifier(random_state=1)
clf_all.fit(X_train, y_train)

y_pred_rf_all=clf_all.predict(X_test)
X_test['out']=y_pred_rf_all
X_test['out_real']=y_test
dump(clf_all, '/Users/verge/Desktop/healthcare/add_model.joblib')

X_test.to_csv('/Users/verge/Desktop/seizure_model_out.csv')
