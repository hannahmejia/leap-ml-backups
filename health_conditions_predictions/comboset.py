#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar  8 13:15:25 2021

@author: verge
"""

#need S1701_C03_005M, B22001_001E, B22001_003E

import pandas as pd


path='/Users/verge/Desktop/healthcare/CMS_syn/'
df_bene_air_income_snap=pd.read_csv(path+'bene_air_income_snap.csv')

df_bene_air_income_snap=df_bene_air_income_snap[['fipsid','DESYNPUF_ID', 'BENE_BIRTH_DT', 'BENE_DEATH_DT',
       'BENE_SEX_IDENT_CD', 'BENE_RACE_CD', 'BENE_ESRD_IND',
       'SP_STATE_CODE', 'BENE_COUNTY_CD', 'BENE_HI_CVRAGE_TOT_MONS',
       'BENE_SMI_CVRAGE_TOT_MONS', 'BENE_HMO_CVRAGE_TOT_MONS',
       'PLAN_CVRG_MOS_NUM', 'SP_ALZHDMTA', 'SP_CHF', 'SP_CHRNKIDN',
       'SP_CNCR', 'SP_COPD', 'SP_DEPRESSN', 'SP_DIABETES', 'SP_ISCHMCHT',
       'SP_OSTEOPRS', 'SP_RA_OA', 'SP_STRKETIA', 'MEDREIMB_IP',
       'BENRES_IP', 'PPPYMT_IP', 'MEDREIMB_OP', 'BENRES_OP', 'PPPYMT_OP',
       'MEDREIMB_CAR', 'BENRES_CAR', 'PPPYMT_CAR','Days with AQI', 'Good Days', 'Moderate Days',
       'Unhealthy for Sensitive Groups Days', 'Unhealthy Days',
       'Very Unhealthy Days', 'Hazardous Days', 'Max AQI',
       '90th Percentile AQI', 'Median AQI', 'Days CO', 'Days NO2',
       'Days Ozone', 'Days SO2', 'Days PM2.5', 'Days PM10','S1701_C03_005M', 'B22001_001E', 'B22001_003E']]

path1='/Users/verge/Desktop/healthcare/SDOH/states/'

df_sdoh=pd.read_csv(path1+'allcountydata.csv')



df_bene_air_income_snap['bad_air_pct']=((df_bene_air_income_snap['Unhealthy for Sensitive Groups Days']+df_bene_air_income_snap['Unhealthy Days']
        +df_bene_air_income_snap['Very Unhealthy Days']+df_bene_air_income_snap['Hazardous Days'])/df_bene_air_income_snap['Days with AQI'])*100

df_bene_air_income_snap['snap_pct']=(df_bene_air_income_snap['B22001_003E']/df_bene_air_income_snap['B22001_001E'])*100

df_bene_air_income_snap['bd']= pd.to_datetime(df_bene_air_income_snap['BENE_BIRTH_DT'].astype(str), format='%Y%m%d')
df_bene_air_income_snap['dd']= pd.to_datetime(df_bene_air_income_snap['BENE_DEATH_DT'].astype(str), format='%Y%m%d')
df_bene_air_income_snap['dd']=df_bene_air_income_snap['dd'].fillna(pd.datetime(2010,12,31))
df_bene_air_income_snap['age']=(df_bene_air_income_snap['dd']-df_bene_air_income_snap['bd']).astype('<m8[Y]') 

df_bene_air_income_snap['diabetes']=df_bene_air_income_snap['SP_DIABETES'].replace(2,0)
df_bene_air_income_snap['depresssion']=df_bene_air_income_snap['SP_DEPRESSN'].replace(2,0)
df_bene_air_income_snap['alz']=df_bene_air_income_snap['SP_ALZHDMTA'].replace(2,0)
df_bene_air_income_snap['copd']=df_bene_air_income_snap['SP_COPD'].replace(2,0)
df_bene_old1=df_bene_air_income_snap[df_bene_air_income_snap['age']>64]

df_bene_old=pd.merge(df_bene_old1,df_sdoh,how='left',left_on='fipsid',right_on='FIPS',indicator=True)
df_bene_old=df_bene_old[df_bene_old['_merge']=='both']

df_bene_old=df_bene_old[['fipsid','DESYNPUF_ID', 'BENE_BIRTH_DT', 'BENE_DEATH_DT',
       'BENE_SEX_IDENT_CD', 'BENE_RACE_CD', 'BENE_ESRD_IND',
       'SP_STATE_CODE', 'BENE_COUNTY_CD', 'BENE_HI_CVRAGE_TOT_MONS',
       'BENE_SMI_CVRAGE_TOT_MONS', 'BENE_HMO_CVRAGE_TOT_MONS',
       'PLAN_CVRG_MOS_NUM', 'SP_ALZHDMTA', 'SP_CHF', 'SP_CHRNKIDN',
       'SP_CNCR', 'SP_COPD', 'SP_DEPRESSN', 'SP_DIABETES', 'SP_ISCHMCHT',
       'SP_OSTEOPRS', 'SP_RA_OA', 'SP_STRKETIA', 'MEDREIMB_IP',
       'BENRES_IP', 'PPPYMT_IP', 'MEDREIMB_OP', 'BENRES_OP', 'PPPYMT_OP',
       'MEDREIMB_CAR', 'BENRES_CAR', 'PPPYMT_CAR','Days with AQI', 'Good Days', 'Moderate Days',
       'Unhealthy for Sensitive Groups Days', 'Unhealthy Days',
       'Very Unhealthy Days', 'Hazardous Days', 'Max AQI',
       '90th Percentile AQI', 'Median AQI', 'Days CO', 'Days NO2',
       'Days Ozone', 'Days SO2', 'Days PM2.5', 'Days PM10','S1701_C03_005M', 'B22001_001E', 'B22001_003E',
       '% Smokers','% Adults with Obesity','Presence of Water Violation','% Severe Housing Problems','age','snap_pct','bad_air_pct','diabetes','alz','depresssion','copd']]

import numpy as np
 
df_bene_old['smoker']=0
df_bene_old['obese']=0


for i in range(len(df_bene_old)):
    df_bene_old['smoker'][i]=np.random.choice([0,1],1,p=[(1-(df_bene_old['% Smokers'][i]/100)),(df_bene_old['% Smokers'][i]/100)])
    df_bene_old['obese'][i]=np.random.choice([0,1],1,p=[(1-(df_bene_old['% Adults with Obesity'][i]/100)),(df_bene_old['% Adults with Obesity'][i]/100)])


df_fastfood=pd.read_csv('/Users/verge/Desktop/healthcare/SDOH/restaurants_county.csv')

df_bene_old2=pd.merge(df_bene_old,df_fastfood,how='left',left_on='fipsid',right_on='FIPS',indicator=True)
df_bene_old2=df_bene_old2[df_bene_old2['_merge']=='both']

df_bene_old=df_bene_old2[['fipsid','DESYNPUF_ID', 'BENE_BIRTH_DT', 'BENE_DEATH_DT',
       'BENE_SEX_IDENT_CD', 'BENE_RACE_CD', 'BENE_ESRD_IND',
       'SP_STATE_CODE', 'BENE_COUNTY_CD', 'BENE_HI_CVRAGE_TOT_MONS',
       'BENE_SMI_CVRAGE_TOT_MONS', 'BENE_HMO_CVRAGE_TOT_MONS',
       'PLAN_CVRG_MOS_NUM', 'SP_ALZHDMTA', 'SP_CHF', 'SP_CHRNKIDN',
       'SP_CNCR', 'SP_COPD', 'SP_DEPRESSN', 'SP_DIABETES', 'SP_ISCHMCHT',
       'SP_OSTEOPRS', 'SP_RA_OA', 'SP_STRKETIA', 'MEDREIMB_IP',
       'BENRES_IP', 'PPPYMT_IP', 'MEDREIMB_OP', 'BENRES_OP', 'PPPYMT_OP',
       'MEDREIMB_CAR', 'BENRES_CAR', 'PPPYMT_CAR','Days with AQI', 'Good Days', 'Moderate Days',
       'Unhealthy for Sensitive Groups Days', 'Unhealthy Days',
       'Very Unhealthy Days', 'Hazardous Days', 'Max AQI',
       '90th Percentile AQI', 'Median AQI', 'Days CO', 'Days NO2',
       'Days Ozone', 'Days SO2', 'Days PM2.5', 'Days PM10','S1701_C03_005M', 'B22001_001E', 'B22001_003E',
       'smoker','obese','Presence of Water Violation','% Severe Housing Problems','FFRPTH11','age','snap_pct','bad_air_pct','diabetes','alz','depresssion','copd']]

df_bene_old['Presence of Water Violation'].replace({'Yes':1,'No':0},inplace=True)


# df_bene_old['diabetes']=df_bene_old['SP_DIABETES'].replace(2,0)
# df_bene_old['depresssion']=df_bene_old['SP_DEPRESSN'].replace(2,0)
# df_bene_old['alz']=df_bene_old['SP_ALZHDMTA'].replace(2,0)
df_bene_old_yes=df_bene_old[df_bene_old['diabetes']==1]
df_bene_old_no=df_bene_old[df_bene_old['diabetes']==0]
df_bene_old_no=df_bene_old_no[0:len(df_bene_old_yes)]
df_bene_old_new=df_bene_old_yes.append(df_bene_old_no)
df_bene_old_new = df_bene_old_new.sample(frac=1).reset_index(drop=True)


df_bene_old_yes_copd=df_bene_old[df_bene_old['copd']==1]
df_bene_old_no_copd=df_bene_old[df_bene_old['copd']==0]
df_bene_old_no_copd=df_bene_old_no_copd[0:len(df_bene_old_yes_copd)]
df_bene_old_new_copd=df_bene_old_yes_copd.append(df_bene_old_no_copd)
df_bene_old_new_copd= df_bene_old_new_copd.sample(frac=1).reset_index(drop=True)


df_bene_old_yes_alz=df_bene_old[df_bene_old['alz']==1]
df_bene_old_no_alz=df_bene_old[df_bene_old['alz']==0]
df_bene_old_no_alz=df_bene_old_no_alz[0:len(df_bene_old_yes_alz)]
df_bene_old_new_alz=df_bene_old_yes_alz.append(df_bene_old_no_alz)
df_bene_old_new_alz= df_bene_old_new_alz.sample(frac=1).reset_index(drop=True)


df_bene_old_yes_dep=df_bene_old[df_bene_old['depresssion']==1]
df_bene_old_no_dep=df_bene_old[df_bene_old['depresssion']==0]
df_bene_old_no_dep=df_bene_old_no_dep[0:len(df_bene_old_yes_dep)]
df_bene_old_new_dep=df_bene_old_yes_dep.append(df_bene_old_no_dep)
df_bene_old_new_dep= df_bene_old_new_dep.sample(frac=1).reset_index(drop=True)


cols=['BENE_RACE_CD','BENE_SEX_IDENT_CD','smoker','obese','age','S1701_C03_005M','snap_pct','bad_air_pct','Presence of Water Violation','% Severe Housing Problems','FFRPTH11']
X=df_bene_old_new[cols]
X['Presence of Water Violation'].replace(np.NaN, 0, inplace=True)
y=df_bene_old_new[['diabetes']]

from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split

from joblib import dump, load

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=0)

clf_all = RandomForestClassifier(random_state=0,min_samples_leaf=5,min_samples_split=5)
clf_all.fit(X_train, y_train)

y_pred_rf_all=clf_all.predict(X_test)

dump(clf_all, '/Users/verge/Desktop/healthcare/medicare_model.joblib')

# import matplotlib.pyplot as plt
# import seaborn as sns
# sns_plt=sns.pairplot(X)0.95
# plt.savefig('output.png')

from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split


import statsmodels.api as sm
logit_model=sm.Logit(y,X)
result=logit_model.fit()
print(result.summary2())

X_train, X_test, y_train, y_test_diabetes = train_test_split(X, y, test_size=0.3, random_state=0)

logreg = LogisticRegression(max_iter=1000)
logreg.fit(X_train, y_train)

y_pred_diabetes = logreg.predict(X_test)
print('Accuracy of logistic regression classifier on test set: {:.2f}'.format(logreg.score(X_test, y_test_diabetes)))

from sklearn.ensemble import RandomForestClassifier
clf_dia = RandomForestClassifier(random_state=0,min_samples_leaf=5,min_samples_split=5)
clf_dia.fit(X_train, y_train)

y_pred_rf_diabetes=clf_dia.predict(X_test)

from joblib import dump, load
dump(clf_dia, '/Users/verge/Desktop/diabetes.joblib')


# clf2 = pickle.loads(s)
# clf2.predict(X[0:1])

print('Accuracy of RF classifier on test set: {:.2f}'.format(clf_dia.score(X_test, y_test_diabetes)))

##COPD
X_copd=df_bene_old_new_copd[cols]
X_copd['Presence of Water Violation'].replace(np.NaN, 0, inplace=True)
y_copd=df_bene_old_new_copd['copd']

logit_model=sm.Logit(y_copd,X_copd)
result=logit_model.fit()
print(result.summary2())

X_train, X_test_copd, y_train, y_test_copd = train_test_split(X_copd, y_copd, test_size=0.3, random_state=0)

logreg = LogisticRegression(max_iter=1000)
logreg.fit(X_train, y_train)

y_pred_copd = logreg.predict(X_test)
print('Accuracy of logistic regression classifier on test set: {:.2f}'.format(logreg.score(X_test, y_test_copd)))

from sklearn.ensemble import RandomForestClassifier
clf_copd = RandomForestClassifier(random_state=0,min_samples_leaf=5,min_samples_split=5)
clf_copd.fit(X_train, y_train)

y_pred_rf_copd=clf_copd.predict(X_test_copd)
dump(clf_copd, '/Users/verge/Desktop/copd.joblib')


print('Accuracy of RF classifier on test set: {:.2f}'.format(clf_copd.score(X_test, y_test_copd)))


##ALZ
X_alz=df_bene_old_new_alz[cols]
X_alz['Presence of Water Violation'].replace(np.NaN, 0, inplace=True)
y_alz=df_bene_old_new_alz['alz']

logit_model=sm.Logit(y_alz,X_alz)
result=logit_model.fit()
print(result.summary2())

X_train, X_test_alz, y_train, y_test_alz = train_test_split(X_alz, y_alz, test_size=0.3, random_state=0)

logreg = LogisticRegression(max_iter=1000)
logreg.fit(X_train, y_train)

y_pred_alz = logreg.predict(X_test)
print('Accuracy of logistic regression classifier on test set: {:.2f}'.format(logreg.score(X_test, y_test_alz)))

from sklearn.ensemble import RandomForestClassifier
clf_alz = RandomForestClassifier(random_state=0,min_samples_leaf=5,min_samples_split=5)
clf_alz.fit(X_train, y_train)

y_pred_rf_alz=clf_alz.predict(X_test_alz)
dump(clf_alz, '/Users/verge/Desktop/alz.joblib')

print('Accuracy of RF classifier on test set: {:.2f}'.format(clf_alz.score(X_test, y_test_alz)))

##Dep
X_dep=df_bene_old_new_dep[cols]
X_dep['Presence of Water Violation'].replace(np.NaN, 0, inplace=True)
y_dep=df_bene_old_new_dep['depresssion']

logit_model=sm.Logit(y_dep,X_dep)
result=logit_model.fit()
print(result.summary2())

X_train, X_test_dep, y_train, y_test_dep = train_test_split(X_dep, y_dep, test_size=0.3, random_state=0)

logreg = LogisticRegression(max_iter=1000)
logreg.fit(X_train, y_train)

y_pred_dep = logreg.predict(X_test)
print('Accuracy of logistic regression classifier on test set: {:.2f}'.format(logreg.score(X_test, y_test_dep)))

from sklearn.ensemble import RandomForestClassifier
clf_dep = RandomForestClassifier(random_state=0,min_samples_leaf=5,min_samples_split=5)
clf_dep.fit(X_train, y_train)

y_pred_rf_dep=clf_dep.predict(X_test_dep)

dump(clf_dep, '/Users/verge/Desktop/depression.joblib')


print('Accuracy of RF classifier on test set: {:.2f}'.format(clf_dep.score(X_test, y_test_dep)))
